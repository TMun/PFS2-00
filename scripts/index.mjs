import { scalingHelper } from "./scalingHelper.mjs";

const moduleId = "tnt-pf2e-pfs02";
const apTitle = "TNT PF2e PFS02";
const apSlug = apTitle.replaceAll(/[^0-9a-zA-Z]/g, "");
console.log(`TNT200 | setting up scaler for ${apSlug}`);
window.scalingHelper = window.scalingHelper ?? {};
window.scalingHelper[`${apSlug.toLowerCase()}`] = {
  scalingHelper,
};

// Import macro is one simple mysterious line
// window.scalingHelper['tnt-pf2e-pfs02'].scalingHelper()

Hooks.once("init", () => {
  DocumentSheetConfig.registerSheet(
    Adventure,
    "tnt-pfs0200-assets",
    DrentalPF2EAdventureImporter,
    {
      label: `TNT PFS 2-00 Asset Pack Importer`,
      makeDefault: false,
    }
  );

  DocumentSheetConfig.registerSheet(
    JournalEntry,
    "tnt-pfs0200-assets",
    DrentalPF2EJournalSheet,
    {
      type: "base",
      makeDefault: false,
      canBeDefault: true,
      label: `TNT PFS 2-00 Asset Pack Journal`,
    }
  );

  DocumentSheetConfig.registerSheet(
    JournalEntryPage,
    "tnt-pfs0200-assets",
    DrentalPF2EJournalSheetPage,
    {
      type: "text",
      makeDefault: false,
      canBeDefault: true,
      label: `TNT PFS 2-00 Asset Pack Journal`,
    }
  );
});

class DrentalPF2EJournalSheet extends JournalSheet {
  static get defaultOptions() {
    const overrides = {
      classes: ["sheet", "journal-sheet", "journal-entry", `2-00-wrapper`],
      width: window.innerWidth < 800 ? 720 : 960,
      height: window.innerHeight < 1000 ? 700 : 800,
    };
    return foundry.utils.mergeObject(super.defaultOptions, overrides);
  }

  getData(options) {
    const data = super.getData(options);
    if (this?.document?.flags["tnt-pfs0200-assets"]?.tier) {
      data.cssClass += ` ${this.document.flags["tnt-pfs0200-assets"].tier}`;
    }
    return data;
  }
}

class DrentalPF2EJournalSheetPage extends JournalTextPageSheet {
  async showWhisperDialog(doc, content) {
    if (!(doc instanceof JournalEntry || doc instanceof JournalEntryPage))
      return;
    if (!doc.isOwner)
      return ui.notifications.error("JOURNAL.ShowBadPermissions", {
        localize: true,
      });

    return await Dialog.wait({
      title: "Send Read-aloud",
      content: "Do you want to send this text to chat?",
      buttons: {
        yes: {
          label: "Yes!",
          callback: async () => {
            return ChatMessage.create({
              content: content,
            });
          },
        },
        no: {
          label: "No! Abort!",
        },
      },
    });
  }

  async _onClickReadAloud(event) {
    event.preventDefault();
    if (["IMG", "A"].includes(event.target.tagName)) return;
    const el = event.currentTarget;
    const readAloudHTML = `<div data-chatable>${el.innerHTML}</div>`;
    // const journal = new JournalEntry();
    this.showWhisperDialog(this.object.parent, readAloudHTML);
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find(".read-aloud").click(this._onClickReadAloud.bind(this));
  }
}

class DrentalPF2EAdventureImporter extends AdventureImporter {
  /**
   *  Add adventure stuff
   *
   * @param {Adventure} adventure
   * @param {object} options
   */
  constructor(adventure, options) {
    super(adventure, options);
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  async getData(options = {}) {
    const data = await super.getData();
    return data;
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  activateListeners(html) {
    super.activateListeners(html);
  }

  /* -------------------------------------------- */

  /**
   * Prepare a list of content types provided by this adventure.
   *
   * @returns {{icon: string, label: string, count: number}[]}
   * @protected
   */
  _getContentList() {
    return Object.entries(Adventure.contentFields).reduce(
      (arr, [field, cls]) => {
        const count = this.adventure[field].size;
        if (!count) return arr;
        arr.push({
          field,
          icon: CONFIG[cls.documentName].sidebarIcon,
          label: game.i18n.localize(
            count > 1 ? cls.metadata.labelPlural : cls.metadata.label
          ),
          count,
        });
        return arr;
      },
      []
    );
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  async _prepareImportData(formData) {
    this.submitOptions = formData;
    const { toCreate, toUpdate, documentCount } =
      await super._prepareImportData(formData);
    if ("Actor" in toCreate) await this.#mergeCompendiumActors(toCreate.Actor);
    if ("Actor" in toUpdate) await this.#mergeCompendiumActors(toUpdate.Actor);
    return { toCreate, toUpdate, documentCount };
  }

  /* -------------------------------------------- */

  /** @inheritDoc */
  async _importContent(toCreate, toUpdate, documentCount) {
    const importResult = await super._importContent(
      toCreate,
      toUpdate,
      documentCount
    );
    game.user.assignHotbarMacro(game.macros.get("4kFBzjFpu7LhKh3V"), 1, {});
    return importResult;
  }

  /* -------------------------------------------- */

  /**
   * Merge Actor data with authoritative source data from system compendium packs
   *
   * @param {Actor[]} actors        Actor documents intended to be imported
   * @param {object} importOptions  Form submission import options
   * @returns {Promise<void>}
   */
  async #mergeCompendiumActors(actors) {
    for (let actor of actors) {
      const [, packageId, compendiumId, type, documentId] =
        actor.flags?.core?.sourceId?.split?.(".") ?? [];
      const pack = game.packs.get(`${packageId}.${compendiumId}`);
      // new style of UUID has an extra element, use the last element regardless of the UUID version
      if (!pack?.index?.has?.(documentId ? documentId : type)) {
        if (pack) {
          console.warn(
            `[${packageId}] Compendium source data for "${actor.name}" [${actor._id}] not found in pack ${pack?.collection}`
          );
        }
        continue;
      }
      const source = await pack.getDocument(documentId);
      const sourceData = source.toObject();

      if (source.type === "npc") {
        actor = Object.assign(
          actor,
          foundry.utils.mergeObject(sourceData, {
            folder: actor.folder,
            img: actor.img,
            name: actor.name,
            "prototypeToken.name": actor.prototypeToken.name,
            "prototypeToken.texture": actor.prototypeToken.texture,
            "system.attributes.adjustment": actor.system.attributes.adjustment,
            "system.details.alignment": actor.system.details.alignment,
            "system.details.blurb": actor.system.details.blurb,
            "system.attributes.hp.value": actor.system.attributes.hp.value,
            "system.traits.languages.value":
              actor.system.traits.languages.value,
            "system.traits.value": actor.system.traits.value,
            _id: actor._id,
          })
        );
      }
      if (source.type === "hazard") {
        actor = Object.assign(
          actor,
          foundry.utils.mergeObject(sourceData, {
            folder: actor.folder,
            img: actor.img,
            name: actor.name,
            "prototypeToken.name": actor.prototypeToken.name,
            "prototypeToken.texture": actor.prototypeToken.texture,
            "system.traits.value": actor.system.traits.value,
            _id: actor._id,
          })
        );
      }
    }
  }
}
