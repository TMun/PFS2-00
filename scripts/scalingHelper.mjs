import { scenarioDefinitionFiles } from "./scenarioDefinitions/scenarioFileList.mjs";
var scenarioDefinitions = [];
for (let filename of scenarioDefinitionFiles) {
  await import(`./scenarioDefinitions/${filename}`)
    .then((module) => {
      scenarioDefinitions.push({
        data: module.default,
        label: module.default.scenarioLabel,
      });
    })
    .catch((error) => console.log(error));
}

export async function scalingHelper() {
  let selection = {};
  const setSelection = async (html) => {
    const scenarioLabel = html[0].querySelector("#scenario option:checked").value;
    selection = scenarioDefinitions.find((s) => s.label == scenarioLabel).data;
  };
  let content =
    scenarioDefinitions.reduce((acc, s) => {
      return acc + `<option value="${s.label}">${s.label}</option>`;
    }, `<label for="scenario">Choose a Scenario:</label><select name="scenario" id="scenario">`) + "</select>";

  await Dialog.wait({
    title: "Scenario Selection",
    content: content,
    buttons: {
      ok: { label: "OK", callback: (html) => setSelection(html) },
      cancel: { label: "Cancel" },
    },
    default: "ok",
  });

  if (!selection.scenarioMinLevel) {
    return;
  }

  // the scalingOptions list is very standard. Including it in the definition file is just super repetitive.
  selection.scalingOptions = [
    {
      tier: `${selection.scenarioMinLevel}-${selection.scenarioMinLevel + 1}`,
      minCP: 8,
      maxCP: 18,
    },
    {
      tier: `${selection.scenarioMinLevel + 2}-${selection.scenarioMinLevel + 3}`,
      minCP: 16,
      maxCP: 36,
    },
  ];

  // if we have tokens selected, we can attempt to calculate Challenge Points
  let challengePoints = 0;
  let numberOfPCs = 0;
  if (canvas.tokens.controlled) {
    // try to determine if a token is a main character or something else by comparing each actor's class to the list of PF2e classes
    const CLASSES = game.packs.get("pf2e.classes");
    const CPMap = {};
    CPMap[selection.scenarioMinLevel] = 2;
    CPMap[selection.scenarioMinLevel + 1] = 3;
    CPMap[selection.scenarioMinLevel + 2] = 4;
    CPMap[selection.scenarioMinLevel + 3] = 6;

    for (const t of canvas.tokens.controlled) {
      if (t.actor.type == "character") {
        if (CLASSES.index.find((c) => c.name == t.actor.class?.name)) {
          if (t.actor.level < selection.scenarioMinLevel || t.actor.level > selection.scenarioMinLevel + 3) {
            ui.notifications.error(
              `${t.actor.name} is level ${t.actor.level} which is out of level range for this scenario.`
            );
            return -1;
          } else {
            numberOfPCs++;
            challengePoints = challengePoints + CPMap[t.actor.level];
          }
        } else {
          console.log(
            `TNT200 | ${t.actor.name}'s class, ${t.actor.class?.name}, is not a main character class. Not including them in the CP count.`
          );
        }
      }
    }
  }

  // The radio buttons to choose the number of players
  if (numberOfPCs < 4) {
    numberOfPCs = 4;
  }
  content = "<label>Number of PCs: </label>";
  for (let l = 4; l <= 6; l++) {
    const checked = l == numberOfPCs ? "checked" : "";
    content =
      content +
      `<input type="radio" name="numpcs" value="${l}" id="${l}" ${checked}><label for="${l}"> ${l}</label>&nbsp;`;
  }
  // The input field for the Challenge Points
  content =
    content +
    `<hr><label for="cp">Challenge Points:</label><input type="number" inputmode="numeric" id="cp" name="cp" min="8" max="36" length="2" value="${
      challengePoints || 8
    }" required></input>
    </form>`;

  if (selection.variations) {
    content += `<div class="form-group"><label>Variations:</label>
    <p>`;
    for (const variation of selection.variations) {
      const roll = new Roll(`1d${variation.options.length}`);
      await roll.evaluate();
      content +=
        `<label>${variation?.name}: </label><br />` +
        variation?.options
          .map(
            (o, i) =>
              `<input type="radio" name="${variation?.name}" value="${o}" id="${o}"${
                i + 1 === Number(roll.result) ? " checked" : ""
              }><label for="${o}"> ${o}</label><br />
                `
          )
          .join("");
      content += "</p><p>";
    }
    content += `</p>
    </div>`;
  }
  await Dialog.wait({
    title: "Scaling Selection",
    content: content,
    buttons: {
      ok: { label: "OK", callback: (html) => doIt(html, selection) },
      cancel: { label: "Cancel" },
    },
    default: "ok",
  });
}

async function doIt(html, selection) {
  console.log(`TNT200 | selection: `, selection);
  const scaling = { cp: 0, number_of_pcs: 4, tier: "" };
  // save the challenge points
  scaling.cp = Number(html[0].querySelector("#cp").value);
  // save the number of PCs
  for (let n = 4; n <= 6; n++) {
    if (html[0].querySelector(`[name="numpcs"][value="${n}"]`).checked) {
      scaling.number_of_pcs = n;
    }
  }
  if (scaling.cp < 16 || (scaling.number_of_pcs >= 5 && scaling.cp <= 18)) {
    scaling.tier = `${selection.scenarioMinLevel}-${selection.scenarioMinLevel + 1}`;
  } else {
    scaling.tier = `${selection.scenarioMinLevel + 2}-${selection.scenarioMinLevel + 3}`;
  }
  // rudimentary input verification
  if (scaling.cp == undefined) {
    ui.notifications.error("Please enter a Challenge Points value.");
    return;
  }
  scaling.variations = [];
  if (selection.variations) {
    for (const variation of selection.variations) {
      for (const option of variation?.options) {
        if (html[0].querySelector(`[name="${variation?.name}"][value="${option}"]`).checked) {
          scaling.variations?.push({ name: variation?.name, option });
        }
      }
    }
  }

  console.log("TNT200 | scaling", scaling);
  // set up the CSS to only show the relevant tier of information
  const additionalCssClass = [
    `journal-tier${scaling.tier}`,
    `journal-party-${scaling.number_of_pcs == 4 ? "four" : scaling.number_of_pcs == 5 ? "five" : "six"}`,
  ].join(" ");
  console.log("TNT200 | css ", additionalCssClass);
  JournalEntry.updateDocuments(
    selection.journalEntries.map((j) => ({
      _id: j,
      "flags.tnt-pfs0200-assets.tier": additionalCssClass,
    }))
  );

  console.log("TNT200 | selection", selection);
  for (const opt of selection.scalingOptions) {
    if (opt.tier == scaling.tier) {
      if (scaling.cp < opt.minCP || scaling.cp > opt.maxCP) {
        ui.notifications.error(
          `The specified Challenge Points (${scaling.cp}) is not in range for levels ${scaling.tier} (${opt.minCP} - ${opt.maxCP})`
        );
        return;
      }
    }
  }

  // Clear all Combats, GMs always forget that
  Combat.deleteDocuments([], { deleteAll: true });

  // Prepare to place player tokens on the maps
  let playerTokenInfo = null;
  let playerMinX = null;
  let playerMinY = null;
  if (canvas.tokens.controlled.length == 0) {
    // it's legit to not select player tokens. Log a console message to show we recognized it.
    console.log("TNT200 | No tokens selected for player placement.");
  } else {
    const currentSceneDPI = canvas.scene.dimensions.size;
    playerTokenInfo = canvas.tokens.controlled.map((t) => ({
      actorID: t.actor._id,
      w: t.hitArea.width / currentSceneDPI,
      h: t.hitArea.height / currentSceneDPI,
      x: t.position.x,
      y: t.position.y,
    }));
    // Find the upper left point of the selected tokens
    playerMinX = canvas.scene.dimensions.width;
    playerMinY = canvas.scene.dimensions.height;
    for (const token of playerTokenInfo) {
      if (token.x < playerMinX) {
        playerMinX = token.x;
      }
      if (token.y < playerMinY) {
        playerMinY = token.y;
      }
    }

    // convert player token x,y into relative grid-sized coordinates
    for (const token of playerTokenInfo) {
      token.x = (token.x - playerMinX) / currentSceneDPI;
      token.y = (token.y - playerMinY) / currentSceneDPI;
    }
  }

  // place all tokens
  const feedbackDialog = new Dialog({
    title: "Scenario Scaling and Token Placement",
    content: "",
    buttons: { cancel: { label: "CLOSE" } },
  });
  let sceneCounter = 0;
  for (const sceneData of selection.scenes) {
    sceneCounter++;
    feedbackDialog.data.content = `<p>Scaling ${sceneData.data.name}</p><br/><progress id="progress" max="${selection.scenes.length}" value="${sceneCounter}"></progress>`;
    await feedbackDialog.render(true);

    const scene = game.scenes.get(sceneData.id);
    if (scene) {
      // Clear all tokens
      await scene.deleteEmbeddedDocuments("Token", [], { deleteAll: true });

      // Rename Scene for the scenario
      await scene.update(sceneData.data);

      // Create array of tokens to place
      const tokenData = [];

      // add players to array of tokens
      const newSceneDPI = scene.dimensions.size;
      if (playerTokenInfo && sceneData.playerStartX) {
        for (const token of playerTokenInfo) {
          const actor = await game.actors.get(token.actorID);
          const actorData = {
            x: token.x * newSceneDPI + sceneData.playerStartX,
            y: token.y * newSceneDPI + sceneData.playerStartY,
            hidden: false,
          };
          tokenData.push(await actor.getTokenDocument(actorData));
        }
      }

      // add NPC tokens to the array
      console.log("TNT200 | sceneData", sceneData);
      const scalingData = sceneData.scaling.filter(
        (s) =>
          s.tier == scaling.tier &&
          s.cp.includes(scaling.cp) &&
          (!s.variation || s.option === scaling.variations?.find((c) => c.name === s.variation).option)
      );
      console.log("TNT200 | scalingData", scalingData);
      if (!!scalingData.length) {
        for (const tokenSpec of scalingData.flatMap((s) => s.tokens)) {
          if (
            tokenSpec.variation &&
            tokenSpec.option !== scaling.variations?.find((c) => c.name === tokenSpec.variation).option
          )
            continue;
          let actor = await game.actors.get(tokenSpec.actorId);
          if (!actor) {
            actor = await game.actors.getName(tokenSpec.actorName);
          }
          if (!actor) {
            console.log(`TNT200 | Cannot find ${tokenSpec.actorId}/${tokenSpec.actorName}`);
          } else {
            for (const x of tokenSpec.data) {
              tokenData.push(await actor.getTokenDocument(x));
            }
          }
        }
      }
      // if no player tokens were selected, and the scene does not have any scaling defined,
      // we might not have anything to place
      if (tokenData) {
        await scene.createEmbeddedDocuments("Token", tokenData);
        for (const token of scene.tokens) {
          if (token.flags?.sigil?.conditions) {
            for (const condition of token.flags?.sigil?.conditions) {
              token.actor.toggleCondition(condition);
            }
          }
          if (token.flags?.sigil?.adjustments) {
            if (token.flags?.sigil?.adjustments.includes("elite")) {
              await token.actor.applyAdjustment("elite");
            }
            if (token.flags?.sigil?.adjustments.includes("weak")) {
              await token.actor.applyAdjustment("weak");
            }
          }
          if (token.flags?.sigil?.items) {
            const items = [];
            for (const item of token.flags?.sigil?.items) {
              items.push(await fromUuid(item));
            }
            await token.actor.createEmbeddedDocuments("Item", items);
          }
          if (token.flags?.sigil?.max) {
            if (/^[+-]/.test(token.flags.sigil.max)) {
              console.log("TNT200 | Bumping actor hp");
              await token.actor.update({
                "system.attributes.hp.max": token.actor.system.attributes.hp.max + Number(token.flags.sigil.max),
              });
              await token.actor.update({
                "system.attributes.hp.value": token.actor.system.attributes.hp.value + Number(token.flags.sigil.max),
              });
            } else {
              await token.actor.update({
                "system.attributes.hp.max": Number(token.flags.sigil.max),
              });
              await token.actor.update({
                "system.attributes.hp.value": Number(token.flags.sigil.max),
              });
            }
          } else if (token.flags?.sigil?.hp) {
            if (/^[+]/.test(token.flags.sigil.hp)) {
              await token.actor.update({
                "system.attributes.hp.max": token.actor.system.attributes.hp.max + Number(token.flags.sigil.hp),
              });
              await token.actor.update({
                "system.attributes.hp.value": token.actor.system.attributes.hp.value + Number(token.flags.sigil.hp),
              });
            } else if (/^[-]/.test(token.flags.sigil.hp)) {
              await token.actor.update({
                "system.attributes.hp.value": token.actor.system.attributes.hp.value + Number(token.flags.sigil.hp),
              });
            } else {
              await token.actor.update({
                "system.attributes.hp.value": Number(token.flags.sigil.hp),
              });
            }
          }
        }
      }
    }
  }
  feedbackDialog.data.content = `<p>${selection.scenes.length} scenes prepared.</p>`;
  await feedbackDialog.render(true);
}
