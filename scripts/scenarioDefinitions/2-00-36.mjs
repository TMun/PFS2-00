export default {
  scenarioLabel: "2-00 King in Thorns (3-6)",
  journalEntries: [
    "NMIsMBmsj6envUvU",
    "JjbVZqBQ6RXZShvy",
    "HgCOIgCSi8Argxli",
    "2VgP1ntdIVIksC1r",
    "3kOv8UCJzVEi8PAA",
    "Fifk8I4NVtsE4yp9",
    "QrLLLblyRReMdxIp",
    "qe71oDhuKx1mfe5A",
    "riXtUdUea9QKjXhq",
    "cCGLculeLY1sZcrk",
  ],
  scenarioMinLevel: 3,
  scenes: [
    {
      id: "asulbcUAgA6ewVtT",
      data: {
        name: "Encampment",
        navName: "Encampment",
      },
      playerStartX: 2000,
      playerStartY: 2900,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "iRxEIg1C4XCn9xtN",
              actorName: "Calisro Benarry",
              data: [{ x: 2100, y: 2100, hidden: false }],
            },
            {
              actorId: "GUnb3w2xfHMT9ewc",
              actorName: "Fola Barun",
              data: [{ x: 1800, y: 1900, hidden: false }],
            },
            {
              actorId: "EoaOGLJm2HjBXDwU",
              actorName: "Urwal",
              data: [{ x: 1300, y: 1900, hidden: false }],
            },
            {
              actorId: "S66102Wg5fu54g80",
              actorName: "Valais Durant",
              data: [{ x: 1400, y: 2600, hidden: false }],
            },
            {
              actorId: "FWMbhE4I272H3KL5",
              actorName: "Venture-Captain Bjersig Torrsen",
              data: [{ x: 2000, y: 2600, hidden: false }],
            },
            {
              actorId: "vb6V1RwYdIyPgIDv",
              actorName: "Venture-Captain Oraiah Tolal",
              data: [{ x: 1600, y: 2900, hidden: false }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
          tokens: [
            {
              actorId: "iRxEIg1C4XCn9xtN",
              actorName: "Calisro Benarry",
              data: [{ x: 2100, y: 2100, hidden: false }],
            },
            {
              actorId: "GUnb3w2xfHMT9ewc",
              actorName: "Fola Barun",
              data: [{ x: 1800, y: 1900, hidden: false }],
            },
            {
              actorId: "EoaOGLJm2HjBXDwU",
              actorName: "Urwal",
              data: [{ x: 1300, y: 1900, hidden: false }],
            },
            {
              actorId: "S66102Wg5fu54g80",
              actorName: "Valais Durant",
              data: [{ x: 1400, y: 2600, hidden: false }],
            },
            {
              actorId: "FWMbhE4I272H3KL5",
              actorName: "Venture-Captain Bjersig Torrsen",
              data: [{ x: 2000, y: 2600, hidden: false }],
            },
            {
              actorId: "vb6V1RwYdIyPgIDv",
              actorName: "Venture-Captain Oraiah Tolal",
              data: [{ x: 1600, y: 2900, hidden: false }],
            },
          ],
        },
      ],
    },
    {
      id: "LN0OsuBxqYd35MtA",
      data: {
        name: "A. Mushroom Ring",
        navName: "A. Mushroom Ring",
      },
      playerStartX: 200,
      playerStartY: 1000,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "g9WY4ObAmpdzKlRf",
              actorName: "Mushroom Ring (3-4)",
              data: [{ x: 500, y: 700, hidden: false }],
            },
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [
                { x: 300, y: 200, hidden: true },
                { x: 400, y: 200, hidden: true },
                { x: 500, y: 200, hidden: true },
                { x: 200, y: 200, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
          tokens: [
            {
              actorId: "G5d0p5hR8tK21MrV",
              actorName: "Korred",
              data: [
                { x: 200, y: 200, hidden: true },
                { x: 300, y: 200, hidden: true },
                { x: 400, y: 200, hidden: true },
                { x: 500, y: 200, hidden: true },
                { x: 600, y: 200, hidden: true },
              ],
            },
            {
              actorId: "sPlhlyG2s1Ltx6N0",
              actorName: "Mushroom Ring (5-6)",
              data: [{ x: 500, y: 700, hidden: false }],
            },
          ],
        },
      ],
    },
    {
      id: "Q2UYeYxTiwLSvsUF",
      data: {
        name: "B. It Talks",
        navName: "B. It Talks",
      },
      playerStartX: 1100,
      playerStartY: 2700,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "caA7enxx4ag65TTv",
              actorName: "Giant Gecko",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2300, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "rHvWrZGjYgALHSfN",
              actorName: "Awakened Giant Chameleon",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "caA7enxx4ag65TTv",
              actorName: "Giant Gecko",
              data: [
                { x: 2300, y: 2300, hidden: true },
                { x: 1800, y: 2000, hidden: true },
                { x: 2000, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "rHvWrZGjYgALHSfN",
              actorName: "Awakened Giant Chameleon",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "caA7enxx4ag65TTv",
              actorName: "Giant Gecko",
              data: [
                { x: 2000, y: 1800, hidden: true },
                { x: 1800, y: 2000, hidden: true },
                { x: 2300, y: 2300, hidden: true },
                { x: 2300, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "rHvWrZGjYgALHSfN",
              actorName: "Awakened Giant Chameleon",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "rHvWrZGjYgALHSfN",
              actorName: "Awakened Giant Chameleon",
              data: [
                { x: 1900, y: 1800, hidden: true },
                { x: 1900, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "caA7enxx4ag65TTv",
              actorName: "Giant Gecko",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2300, y: 2300, hidden: true },
                { x: 2300, y: 1900, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "caA7enxx4ag65TTv",
              actorName: "Giant Gecko",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2300, y: 2300, hidden: true },
                { x: 2300, y: 1900, hidden: true },
                { x: 2100, y: 1700, hidden: true },
                { x: 2200, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "rHvWrZGjYgALHSfN",
              actorName: "Awakened Giant Chameleon",
              data: [
                { x: 1900, y: 2200, hidden: true },
                { x: 1900, y: 1800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "JIoQcpJEKVF78WKA",
              actorName: "Awakened Giant Frilled Lizard",
              data: [{ x: 2000, y: 2300, hidden: true }],
            },
            {
              actorId: "TJM4eFGopvbQafCI",
              actorName: "Giant Chameleon",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2200, y: 1800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "JIoQcpJEKVF78WKA",
              actorName: "Awakened Giant Frilled Lizard",
              data: [{ x: 2000, y: 2300, hidden: true }],
            },
            {
              actorId: "TJM4eFGopvbQafCI",
              actorName: "Giant Chameleon",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2200, y: 1800, hidden: true },
                { x: 1900, y: 1700, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "TJM4eFGopvbQafCI",
              actorName: "Giant Chameleon",
              data: [
                { x: 1900, y: 1700, hidden: true },
                { x: 1800, y: 2000, hidden: true },
                { x: 2200, y: 1800, hidden: true },
                { x: 2200, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "JIoQcpJEKVF78WKA",
              actorName: "Awakened Giant Frilled Lizard",
              data: [{ x: 2000, y: 2300, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "JIoQcpJEKVF78WKA",
              actorName: "Awakened Giant Frilled Lizard",
              data: [
                { x: 2000, y: 2300, hidden: true },
                { x: 1900, y: 1700, hidden: true },
              ],
            },
            {
              actorId: "TJM4eFGopvbQafCI",
              actorName: "Giant Chameleon",
              data: [
                { x: 1800, y: 2000, hidden: true },
                { x: 2200, y: 1800, hidden: true },
                { x: 2200, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "TJM4eFGopvbQafCI",
              actorName: "Giant Chameleon",
              data: [
                { x: 2100, y: 2000, hidden: true },
                { x: 2200, y: 1800, hidden: true },
                { x: 1800, y: 2000, hidden: true },
                { x: 2200, y: 1600, hidden: true },
                { x: 2200, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "JIoQcpJEKVF78WKA",
              actorName: "Awakened Giant Frilled Lizard",
              data: [
                { x: 1900, y: 1700, hidden: true },
                { x: 1900, y: 2300, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "wxnD3jXUojrqLuA1",
      data: {
        name: "C. Befuddled Pathfinders (7-8)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "FVcQs8dVsf1CtEpo",
      data: {
        name: "C. Befuddled Pathfinders (1-2)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "h78Uw5F8Bi8qEZNx",
      data: {
        name: "C. Befuddled Pathfinders (3-6)",
        navName: "C. Befuddled Pathfinders",
        navigation: true,
      },
      playerStartX: 1000,
      playerStartY: 1400,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9, 10, 11],
          tokens: [
            {
              actorId: "5HnT9UsQ7mC5xbvx",
              actorName: "Dancer",
              data: [{ x: 1800, y: 1500, hidden: true }],
            },
            {
              actorId: "HjJLxESb4V3vEMYK",
              actorName: "Guard",
              data: [{ x: 1700, y: 1600, hidden: true }],
            },
            {
              actorId: "OI0kuGWcZ8BOutQW",
              actorName: "Acolyte of Nethys",
              data: [{ x: 1600, y: 1400, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "5HnT9UsQ7mC5xbvx",
              actorName: "Dancer",
              data: [{ x: 1800, y: 1500, hidden: true }],
            },
            {
              actorId: "OI0kuGWcZ8BOutQW",
              actorName: "Acolyte of Nethys",
              data: [{ x: 1600, y: 1400, hidden: true }],
            },
            {
              actorId: "HjJLxESb4V3vEMYK",
              actorName: "Guard",
              data: [
                { x: 1800, y: 1400, hidden: true },
                { x: 1700, y: 1600, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "5HnT9UsQ7mC5xbvx",
              actorName: "Dancer",
              data: [
                { x: 1800, y: 1600, hidden: true },
                { x: 1600, y: 1500, hidden: true },
              ],
            },
            {
              actorId: "OI0kuGWcZ8BOutQW",
              actorName: "Acolyte of Nethys",
              data: [{ x: 1600, y: 1400, hidden: true }],
            },
            {
              actorId: "HjJLxESb4V3vEMYK",
              actorName: "Guard",
              data: [
                { x: 1800, y: 1400, hidden: true },
                { x: 1700, y: 1600, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18, 19, 20, 21, 22],
          tokens: [
            {
              actorId: "0fM8nr5tDPKTAuTy",
              actorName: "Chronicler",
              data: [{ x: 1700, y: 1400, hidden: true }],
            },
            {
              actorId: "D9pQgyEZoFiiFAyD",
              actorName: "Jailer",
              data: [{ x: 1800, y: 1500, hidden: true }],
            },
            {
              actorId: "Ld68pa7EdGpfsH7p",
              actorName: "Tracker",
              data: [{ x: 1600, y: 1600, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27, 28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "D9pQgyEZoFiiFAyD",
              actorName: "Jailer",
              data: [
                { x: 1800, y: 1500, hidden: true },
                { x: 1800, y: 1600, hidden: true },
              ],
            },
            {
              actorId: "0fM8nr5tDPKTAuTy",
              actorName: "Chronicler",
              data: [{ x: 1700, y: 1400, hidden: true }],
            },
            {
              actorId: "Ld68pa7EdGpfsH7p",
              actorName: "Tracker",
              data: [{ x: 1600, y: 1600, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "Ld68pa7EdGpfsH7p",
              actorName: "Tracker",
              data: [
                { x: 1600, y: 1600, hidden: true },
                { x: 1600, y: 1400, hidden: true },
              ],
            },
            {
              actorId: "0fM8nr5tDPKTAuTy",
              actorName: "Chronicler",
              data: [{ x: 1700, y: 1400, hidden: true }],
            },
            {
              actorId: "D9pQgyEZoFiiFAyD",
              actorName: "Jailer",
              data: [
                { x: 1800, y: 1600, hidden: true },
                { x: 1800, y: 1500, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "sLcY8PTEo55sgNhA",
      data: {
        name: "D. Enchanted Plants",
        navName: "D. Enchanted Plants",
      },
      playerStartX: 2000,
      playerStartY: 2600,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2700, y: 2000, hidden: true },
                { x: 2400, y: 1900, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                {
                  x: 2700,
                  y: 2000,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
                {
                  x: 2400,
                  y: 1900,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2400, y: 1900, hidden: true },
                { x: 2700, y: 2000, hidden: true },
              ],
            },
            {
              actorId: "6i3o3I8fpy8i6D2Z",
              actorName: "Fungus Leshy",
              data: [{ x: 2600, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "6i3o3I8fpy8i6D2Z",
              actorName: "Fungus Leshy",
              data: [
                { x: 2700, y: 1800, hidden: true },
                { x: 2600, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2400, y: 1900, hidden: true },
                { x: 2700, y: 2000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2700, y: 2100, hidden: true },
                { x: 2400, y: 1900, hidden: true },
                { x: 2700, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "6i3o3I8fpy8i6D2Z",
              actorName: "Fungus Leshy",
              data: [{ x: 2600, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "fijsjBOtdP6eoerg",
              actorName: "Scythe Tree",
              data: [{ x: 2500, y: 2000, hidden: true }],
            },
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [{ x: 2500, y: 1800, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2400, y: 1800, hidden: true },
                { x: 2700, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "fijsjBOtdP6eoerg",
              actorName: "Scythe Tree",
              data: [{ x: 2500, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2700, y: 2000, hidden: true },
                { x: 2400, y: 1800, hidden: true },
                { x: 2700, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "fijsjBOtdP6eoerg",
              actorName: "Scythe Tree",
              data: [{ x: 2400, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2800, y: 1800, hidden: true },
                { x: 2400, y: 1800, hidden: true },
                { x: 2600, y: 1800, hidden: true },
                { x: 2700, y: 2000, hidden: true },
              ],
            },
            {
              actorId: "fijsjBOtdP6eoerg",
              actorName: "Scythe Tree",
              data: [{ x: 2400, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "fijsjBOtdP6eoerg",
              actorName: "Scythe Tree",
              data: [
                { x: 2700, y: 2000, hidden: true },
                { x: 2400, y: 2000, hidden: true },
              ],
            },
            {
              actorId: "58OsqQ0AKCxG8Rsz",
              actorName: "Snapping Flytrap",
              data: [
                { x: 2400, y: 1800, hidden: true },
                { x: 2600, y: 1800, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "S0JaQ5UdCQ2W3vHR",
      data: {
        name: "E1. Hazards",
        navName: "E1. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "QFR7IfOZ7xhulskg",
      data: {
        name: "E2. Hazards",
        navName: "E2. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "1YtP5qAHQ6iB91Oh",
      data: {
        name: "E3. Hazards",
        navName: "E3. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "D3HvxJV7jROgwrA2",
      data: {
        name: "F. The Disguised Forces",
        navName: "F. The Disguised Forces",
      },
      playerStartX: 1300,
      playerStartY: 2200,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [{ x: 1600, y: 3000, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [
                {
                  x: 1600,
                  y: 3000,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [{ x: 1600, y: 3000, hidden: true }],
            },
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [{ x: 2000, y: 2800, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [
                {
                  x: 1600,
                  y: 3000,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [{ x: 2000, y: 2800, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [{ x: 1600, y: 3000, hidden: true }],
            },
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [
                { x: 2000, y: 3000, hidden: true },
                { x: 2000, y: 2800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [{ x: 1200, y: 1800, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [
                {
                  x: 1200,
                  y: 1800,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [{ x: 1600, y: 3000, hidden: true }],
            },
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [{ x: 1200, y: 1800, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [{ x: 1600, y: 3000, hidden: true }],
            },
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [
                {
                  x: 1200,
                  y: 1800,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "8IkfCEdKDOZP9MdL",
              actorName: "Kelpie",
              data: [
                { x: 2100, y: 2200, hidden: true },
                { x: 1600, y: 3000, hidden: true },
              ],
            },
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [{ x: 1200, y: 1800, hidden: true }],
            },
          ],
        },
      ],
    },
    {
      id: "R9PpAKgFO9j9zumR",
      data: {
        name: "G1. Angry Animals",
        navName: "G1. Angry Animals",
      },
      playerStartX: 1200,
      playerStartY: 2700,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 1900, y: 2700, hidden: true },
                { x: 1700, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                {
                  x: 1900,
                  y: 2700,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 1700,
                  y: 2100,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 1900, y: 2500, hidden: true },
                { x: 1700, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "RGSC0HkS9tqbDYIS",
              actorName: "Hunting Spider",
              data: [{ x: 1900, y: 3500, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "RGSC0HkS9tqbDYIS",
              actorName: "Hunting Spider",
              data: [
                { x: 1900, y: 3500, hidden: true },
                { x: 1000, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 1200, y: 2100, hidden: true },
                { x: 1800, y: 2300, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 2000, y: 2600, hidden: true },
                { x: 1200, y: 2100, hidden: true },
                { x: 1800, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "RGSC0HkS9tqbDYIS",
              actorName: "Hunting Spider",
              data: [
                { x: 1000, y: 1900, hidden: true },
                { x: 1900, y: 3500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "yPK5qWdNNOm7LgTI",
              actorName: "Army Ant Swarm",
              data: [
                { x: 2000, y: 2700, hidden: true },
                { x: 1200, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "nNr6bx6AS5mXPqxj",
              actorName: "Giant Whiptail Centipede",
              data: [{ x: 1700, y: 2200, hidden: true }],
            },
            {
              actorId: "yPK5qWdNNOm7LgTI",
              actorName: "Army Ant Swarm",
              data: [
                { x: 2000, y: 3200, hidden: true },
                { x: 1200, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "yPK5qWdNNOm7LgTI",
              actorName: "Army Ant Swarm",
              data: [
                { x: 2000, y: 3400, hidden: true },
                { x: 1200, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "nNr6bx6AS5mXPqxj",
              actorName: "Giant Whiptail Centipede",
              data: [
                { x: 1700, y: 2200, hidden: true },
                { x: 2200, y: 2700, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "yPK5qWdNNOm7LgTI",
              actorName: "Army Ant Swarm",
              data: [
                { x: 1200, y: 2100, hidden: true },
                { x: 2000, y: 3400, hidden: true },
                { x: 1800, y: 2400, hidden: true },
              ],
            },
            {
              actorId: "nNr6bx6AS5mXPqxj",
              actorName: "Giant Whiptail Centipede",
              data: [{ x: 2200, y: 2700, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "yPK5qWdNNOm7LgTI",
              actorName: "Army Ant Swarm",
              data: [
                { x: 2000, y: 3400, hidden: true },
                { x: 900, y: 2300, hidden: true },
                { x: 1800, y: 2400, hidden: true },
              ],
            },
            {
              actorId: "nNr6bx6AS5mXPqxj",
              actorName: "Giant Whiptail Centipede",
              data: [
                { x: 1600, y: 2100, hidden: true },
                { x: 2200, y: 2700, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "NOrqjfBahDIbmyUX",
      data: {
        name: "G2. Unfortunate Meeting",
        navName: "G2. Unfortunate Meeting",
      },
      playerStartX: 2200,
      playerStartY: 2800,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "CjfMX7U75vd05g3R",
              actorName: "Giant Dragonfly",
              data: [{ x: 2100, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "FZk0IOETekEelAvB",
              actorName: "Giant Cockroach",
              data: [{ x: 1900, y: 2400, hidden: true }],
            },
            {
              actorId: "CjfMX7U75vd05g3R",
              actorName: "Giant Dragonfly",
              data: [{ x: 2100, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "FZk0IOETekEelAvB",
              actorName: "Giant Cockroach",
              data: [
                { x: 1900, y: 2400, hidden: true },
                { x: 2600, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "CjfMX7U75vd05g3R",
              actorName: "Giant Dragonfly",
              data: [{ x: 2100, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "CjfMX7U75vd05g3R",
              actorName: "Giant Dragonfly",
              data: [{ x: 2100, y: 2000, hidden: true }],
            },
            {
              actorId: "FZk0IOETekEelAvB",
              actorName: "Giant Cockroach",
              data: [
                { x: 2600, y: 2200, hidden: true },
                { x: 1900, y: 2400, hidden: true },
                { x: 2800, y: 2500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "CjfMX7U75vd05g3R",
              actorName: "Giant Dragonfly",
              data: [
                { x: 2100, y: 2000, hidden: true },
                { x: 1900, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "FZk0IOETekEelAvB",
              actorName: "Giant Cockroach",
              data: [{ x: 2600, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "p27aQOiRrZukmb9s",
              actorName: "Verdurous Ooze",
              data: [{ x: 2400, y: 2300, hidden: true }],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "p27aQOiRrZukmb9s",
              actorName: "Verdurous Ooze",
              data: [
                {
                  x: 2400,
                  y: 2300,
                  hidden: true,
                  flags: { sigil: { max: "+30" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "p27aQOiRrZukmb9s",
              actorName: "Verdurous Ooze",
              data: [
                {
                  x: 2400,
                  y: 2300,
                  hidden: true,
                  flags: { sigil: { adjustments: ["elite"], max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "p27aQOiRrZukmb9s",
              actorName: "Verdurous Ooze",
              data: [
                { x: 1700, y: 2600, hidden: true },
                { x: 2400, y: 2300, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "p27aQOiRrZukmb9s",
              actorName: "Verdurous Ooze",
              data: [
                {
                  x: 1700,
                  y: 2600,
                  hidden: true,
                  flags: { sigil: { max: "+30" } },
                },
                {
                  x: 2400,
                  y: 2300,
                  hidden: true,
                  flags: { sigil: { max: "+30" } },
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "qrVCa14Z6vyYaw9D",
      data: {
        name: "Opening The Door",
        navName: "Opening The Door",
      },
      playerStartX: 700,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "FUNY7EvFQXUqlmH6",
      data: {
        name: "H. Attunement",
        navName: "H. Attunement",
      },
      playerStartX: 1200,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "2G49yFcdbYTMg5xM",
      data: {
        name: "I. Defense",
        navName: "I. Defense",
      },
      playerStartX: 2300,
      playerStartY: 2500,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "byh1MNlpjx2epmCh",
              actorName: "Satyr",
              data: [{ x: 1400, y: 1900, hidden: true }],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [
                { x: 900, y: 3000, hidden: true },
                { x: 800, y: 1500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "byh1MNlpjx2epmCh",
              actorName: "Satyr",
              data: [
                {
                  x: 1400,
                  y: 1900,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [
                {
                  x: 900,
                  y: 3000,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 800,
                  y: 1500,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "qIBbKSV0ycVVhSdR",
              actorName: "Leprechaun",
              data: [{ x: 1100, y: 1700, hidden: true }],
            },
            {
              actorId: "byh1MNlpjx2epmCh",
              actorName: "Satyr",
              data: [{ x: 1400, y: 1900, hidden: true }],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [
                { x: 1000, y: 1200, hidden: true },
                { x: 800, y: 1500, hidden: true },
                { x: 900, y: 3000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "qIBbKSV0ycVVhSdR",
              actorName: "Leprechaun",
              data: [
                { x: 1100, y: 1700, hidden: true },
                { x: 1400, y: 1600, hidden: true },
              ],
            },
            {
              actorId: "byh1MNlpjx2epmCh",
              actorName: "Satyr",
              data: [{ x: 1400, y: 1900, hidden: true }],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [
                {
                  x: 1000,
                  y: 1200,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 800,
                  y: 1500,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 900,
                  y: 3000,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "byh1MNlpjx2epmCh",
              actorName: "Satyr",
              data: [
                { x: 1400, y: 1900, hidden: true },
                { x: 1100, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [
                { x: 900, y: 3000, hidden: true },
                { x: 900, y: 2800, hidden: true },
                { x: 800, y: 1500, hidden: true },
                { x: 1000, y: 1200, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [{ x: 1400, y: 2000, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 2400, y: 1600, hidden: true },
                { x: 1900, y: 1500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [
                {
                  x: 1400,
                  y: 2000,
                  hidden: true,
                  flags: { sigil: { max: "+20" } },
                },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                {
                  x: 2400,
                  y: 1600,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
                {
                  x: 1900,
                  y: 1500,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "78Lhp1V9COPpqR72",
              actorName: "Pixie (PFS 2-00)",
              data: [{ x: 1100, y: 2300, hidden: true }],
            },
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [{ x: 1400, y: 2000, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 2400, y: 1600, hidden: true },
                { x: 1900, y: 1500, hidden: true },
                { x: 2300, y: 1400, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "78Lhp1V9COPpqR72",
              actorName: "Pixie (PFS 2-00)",
              data: [
                { x: 1300, y: 3100, hidden: true },
                { x: 1100, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [{ x: 1400, y: 2000, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                {
                  x: 2400,
                  y: 1600,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
                {
                  x: 1900,
                  y: 1500,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
                {
                  x: 2300,
                  y: 1400,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [
                { x: 1400, y: 2000, hidden: true },
                { x: 1200, y: 2700, hidden: true },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 2400, y: 1600, hidden: true },
                { x: 1900, y: 1500, hidden: true },
                { x: 2300, y: 1400, hidden: true },
                { x: 2800, y: 1800, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "ydC0BoN4OcUupfHY",
      data: {
        name: "J. The Thorned Monarch Attacks",
        navName: "J. The Thorned Monarch Attacks",
      },
      playerStartX: 1900,
      playerStartY: 1600,
      scaling: [
        {
          tier: "3-4",
          cp: [8, 9],
          tokens: [
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [{ x: 1000, y: 1400, hidden: true }],
            },
            {
              actorId: "QyWGBKXL6g1naewF",
              actorName: "Jinkin",
              data: [
                { x: 1400, y: 2200, hidden: true },
                { x: 2700, y: 2200, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [10, 11],
          tokens: [
            {
              actorId: "QyWGBKXL6g1naewF",
              actorName: "Jinkin",
              data: [
                { x: 1600, y: 1400, hidden: true },
                { x: 2700, y: 2200, hidden: true },
                { x: 1400, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [{ x: 1000, y: 1400, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [12, 13],
          tokens: [
            {
              actorId: "QyWGBKXL6g1naewF",
              actorName: "Jinkin",
              data: [
                { x: 1600, y: 1400, hidden: true },
                { x: 2700, y: 2200, hidden: true },
                { x: 1400, y: 2200, hidden: true },
                { x: 1200, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [{ x: 1000, y: 1400, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [14, 15],
          tokens: [
            {
              actorId: "QyWGBKXL6g1naewF",
              actorName: "Jinkin",
              data: [
                { x: 1200, y: 1900, hidden: true },
                { x: 2700, y: 2200, hidden: true },
                { x: 1400, y: 2200, hidden: true },
                { x: 1600, y: 1400, hidden: true },
                { x: 2800, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [{ x: 1000, y: 1400, hidden: true }],
            },
          ],
        },
        {
          tier: "3-4",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "QyWGBKXL6g1naewF",
              actorName: "Jinkin",
              data: [
                { x: 2700, y: 2200, hidden: true },
                { x: 1200, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1000, y: 1400, hidden: true },
                { x: 2800, y: 1000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "cRF4rLuOYsdmiCDR",
              actorName: "Redcap",
              data: [
                { x: 1200, y: 2000, hidden: true },
                { x: 2600, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "xEkuuhNUaTEdUB6F",
              actorName: "Twigjack",
              data: [
                { x: 2800, y: 2200, hidden: true },
                { x: 1500, y: 1500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [19, 20, 21, 22],
          tokens: [
            {
              actorId: "xEkuuhNUaTEdUB6F",
              actorName: "Twigjack",
              data: [
                { x: 1500, y: 1500, hidden: true },
                { x: 2800, y: 2200, hidden: true },
                { x: 1500, y: 2500, hidden: true },
              ],
            },
            {
              actorId: "cRF4rLuOYsdmiCDR",
              actorName: "Redcap",
              data: [
                { x: 1200, y: 2000, hidden: true },
                { x: 2600, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [23, 24, 25, 26, 27],
          tokens: [
            {
              actorId: "xEkuuhNUaTEdUB6F",
              actorName: "Twigjack",
              data: [
                { x: 1500, y: 2500, hidden: true },
                { x: 2800, y: 2200, hidden: true },
                { x: 1500, y: 1500, hidden: true },
                { x: 2800, y: 1600, hidden: true },
              ],
            },
            {
              actorId: "cRF4rLuOYsdmiCDR",
              actorName: "Redcap",
              data: [
                { x: 1200, y: 2000, hidden: true },
                { x: 2600, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [28, 29, 30, 31, 32],
          tokens: [
            {
              actorId: "cRF4rLuOYsdmiCDR",
              actorName: "Redcap",
              data: [
                { x: 2600, y: 2100, hidden: true },
                { x: 1200, y: 2000, hidden: true },
                { x: 1900, y: 2400, hidden: true },
              ],
            },
            {
              actorId: "xEkuuhNUaTEdUB6F",
              actorName: "Twigjack",
              data: [
                { x: 1500, y: 1500, hidden: true },
                { x: 1500, y: 2500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "5-6",
          cp: [33, 34, 35, 36],
          tokens: [
            {
              actorId: "xEkuuhNUaTEdUB6F",
              actorName: "Twigjack",
              data: [
                { x: 1500, y: 1500, hidden: true },
                { x: 1500, y: 2500, hidden: true },
                { x: 2700, y: 1400, hidden: true },
              ],
            },
            {
              actorId: "cRF4rLuOYsdmiCDR",
              actorName: "Redcap",
              data: [
                { x: 1200, y: 2000, hidden: true },
                { x: 1900, y: 2400, hidden: true },
                { x: 2600, y: 2100, hidden: true },
              ],
            },
          ],
        },
      ],
    },
  ],
};
