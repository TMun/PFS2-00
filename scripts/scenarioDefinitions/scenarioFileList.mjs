// if you add a new scenarioDefinition file, you need to add its name to this list,
// so it is correctly included by scalingHelper function. I wish we could just
// do this automatically, but apparently doing a readdir is the hardest,
// most forbiddenest thing ever :shrug:
export const scenarioDefinitionFiles = ["2-00-12.mjs", "2-00-36.mjs", "2-00-78.mjs"];
