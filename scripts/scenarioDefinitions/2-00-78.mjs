export default {
  scenarioLabel: "2-00 King in Thorns (7-8)",
  journalEntries: [
    "NMIsMBmsj6envUvU",
    "JjbVZqBQ6RXZShvy",
    "HgCOIgCSi8Argxli",
    "2VgP1ntdIVIksC1r",
    "3kOv8UCJzVEi8PAA",
    "Fifk8I4NVtsE4yp9",
    "QrLLLblyRReMdxIp",
    "qe71oDhuKx1mfe5A",
    "riXtUdUea9QKjXhq",
    "cCGLculeLY1sZcrk",
  ],
  scenarioMinLevel: 7,
  scenes: [
    {
      id: "asulbcUAgA6ewVtT",
      data: {
        name: "Encampment",
        navName: "Encampment",
      },
      playerStartX: 2000,
      playerStartY: 2900,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "iRxEIg1C4XCn9xtN",
              actorName: "Calisro Benarry",
              data: [{ x: 2100, y: 2100, hidden: false }],
            },
            {
              actorId: "GUnb3w2xfHMT9ewc",
              actorName: "Fola Barun",
              data: [{ x: 1800, y: 1900, hidden: false }],
            },
            {
              actorId: "EoaOGLJm2HjBXDwU",
              actorName: "Urwal",
              data: [{ x: 1300, y: 1900, hidden: false }],
            },
            {
              actorId: "S66102Wg5fu54g80",
              actorName: "Valais Durant",
              data: [{ x: 1400, y: 2600, hidden: false }],
            },
            {
              actorId: "FWMbhE4I272H3KL5",
              actorName: "Venture-Captain Bjersig Torrsen",
              data: [{ x: 2000, y: 2600, hidden: false }],
            },
            {
              actorId: "vb6V1RwYdIyPgIDv",
              actorName: "Venture-Captain Oraiah Tolal",
              data: [{ x: 1600, y: 2900, hidden: false }],
            },
          ],
        },
      ],
    },
    {
      id: "LN0OsuBxqYd35MtA32",
      data: {
        name: "A. Mushroom Ring",
        navName: "A. Mushroom Ring",
      },
      playerStartX: 200,
      playerStartY: 1000,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "ylJGKbTWKlwEuEPG",
              actorName: "Mushroom Ring (7-8)",
              data: [{ x: 500, y: 700, hidden: true }],
            },
            {
              actorId: "90FH7ed98jDQSIYc",
              actorName: "Elananx",
              data: [
                { x: 300, y: 200, hidden: true },
                { x: 400, y: 200, hidden: true },
                { x: 500, y: 200, hidden: true },
                { x: 600, y: 200, hidden: true },
                { x: 200, y: 200, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "Q2UYeYxTiwLSvsUF",
      data: {
        name: "B. It Talks",
        navName: "B. It Talks",
      },
      playerStartX: 1100,
      playerStartY: 2700,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9],
          tokens: [
            {
              actorId: "2x1C7QZx0hjlmkom",
              actorName: "Giant Frilled Lizard",
              data: [
                { x: 2200, y: 1900, hidden: true },
                { x: 1800, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "UpeRNhu1esf14725",
              actorName: "Awakened Megalania",
              data: [{ x: 1800, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [10, 11],
          tokens: [
            {
              actorId: "2x1C7QZx0hjlmkom",
              actorName: "Giant Frilled Lizard",
              data: [
                { x: 2200, y: 1900, hidden: true },
                { x: 1800, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "UpeRNhu1esf14725",
              actorName: "Awakened Megalania",
              data: [
                {
                  x: 1800,
                  y: 2200,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "2x1C7QZx0hjlmkom",
              actorName: "Giant Frilled Lizard",
              data: [
                { x: 2100, y: 1900, hidden: true },
                { x: 2200, y: 2200, hidden: true },
                { x: 1900, y: 1700, hidden: true },
              ],
            },
            {
              actorId: "UpeRNhu1esf14725",
              actorName: "Awakened Megalania",
              data: [{ x: 1800, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "2x1C7QZx0hjlmkom",
              actorName: "Giant Frilled Lizard",
              data: [
                { x: 2200, y: 1600, hidden: true },
                { x: 2200, y: 2200, hidden: true },
                { x: 1900, y: 1700, hidden: true },
                { x: 2100, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "UpeRNhu1esf14725",
              actorName: "Awakened Megalania",
              data: [{ x: 1800, y: 2200, hidden: true }],
            },
          ],
        },
      ],
    },
    {
      id: "h78Uw5F8Bi8qEZNx",
      data: {
        name: "C. Befuddled Pathfinders (3-6)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "FVcQs8dVsf1CtEpo",
      data: {
        name: "C. Befuddled Pathfinders (1-2)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "wxnD3jXUojrqLuA1",
      data: {
        name: "C. Befuddled Pathfinders (7-8)",
        navName: "C. Befuddled Pathfinders",
        navigation: true,
      },
      playerStartX: 1500,
      playerStartY: 1500,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9, 10, 11],
          tokens: [
            {
              actorId: "79KSZ3hD5wCF1q8z",
              actorName: "Plague Doctor",
              data: [{ x: 1100, y: 1900, hidden: true }],
            },
            {
              actorId: "tLftCmPwAd9ZpFSU",
              actorName: "Tomb Raider",
              data: [{ x: 1100, y: 1700, hidden: true }],
            },
            {
              actorId: "NPi113u2HTIs2mxL",
              actorName: "Fence (PFS 2-00)",
              data: [{ x: 1200, y: 1500, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "tLftCmPwAd9ZpFSU",
              actorName: "Tomb Raider",
              data: [
                { x: 1100, y: 1700, hidden: true },
                { x: 1100, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "NPi113u2HTIs2mxL",
              actorName: "Fence (PFS 2-00)",
              data: [{ x: 1200, y: 1500, hidden: true }],
            },
            {
              actorId: "79KSZ3hD5wCF1q8z",
              actorName: "Plague Doctor",
              data: [{ x: 1100, y: 1900, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "NPi113u2HTIs2mxL",
              actorName: "Fence (PFS 2-00)",
              data: [
                { x: 1200, y: 1500, hidden: true },
                { x: 1200, y: 1600, hidden: true },
              ],
            },
            {
              actorId: "79KSZ3hD5wCF1q8z",
              actorName: "Plague Doctor",
              data: [{ x: 1100, y: 1900, hidden: true }],
            },
            {
              actorId: "tLftCmPwAd9ZpFSU",
              actorName: "Tomb Raider",
              data: [
                { x: 1100, y: 1800, hidden: true },
                { x: 1100, y: 1700, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "sLcY8PTEo55sgNhA",
      data: {
        name: "D. Enchanted Plants",
        navName: "D. Enchanted Plants",
      },
      playerStartX: 2000,
      playerStartY: 2600,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9],
          tokens: [
            {
              actorId: "lRSXcdDNGVEEiym7",
              actorName: "Mandragora",
              data: [
                { x: 2500, y: 2000, hidden: true },
                { x: 2600, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "jGNUUXyayvtHhL2d",
              actorName: "Drainberry Bush",
              data: [{ x: 2700, y: 1900, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [10, 11],
          tokens: [
            {
              actorId: "lRSXcdDNGVEEiym7",
              actorName: "Mandragora",
              data: [
                { x: 2800, y: 2200, hidden: true },
                { x: 2600, y: 2200, hidden: true },
                { x: 2500, y: 2000, hidden: true },
                { x: 2600, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "jGNUUXyayvtHhL2d",
              actorName: "Drainberry Bush",
              data: [{ x: 2700, y: 1900, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13],
          tokens: [
            {
              actorId: "jGNUUXyayvtHhL2d",
              actorName: "Drainberry Bush",
              data: [
                { x: 2700, y: 2000, hidden: true },
                { x: 2600, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "lRSXcdDNGVEEiym7",
              actorName: "Mandragora",
              data: [
                { x: 2600, y: 2200, hidden: true },
                { x: 2500, y: 2000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [14, 15],
          tokens: [
            {
              actorId: "lRSXcdDNGVEEiym7",
              actorName: "Mandragora",
              data: [
                { x: 2600, y: 2000, hidden: true },
                { x: 2400, y: 1900, hidden: true },
                { x: 2500, y: 2100, hidden: true },
                { x: 2600, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "jGNUUXyayvtHhL2d",
              actorName: "Drainberry Bush",
              data: [
                { x: 2600, y: 1800, hidden: true },
                { x: 2700, y: 2000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "lRSXcdDNGVEEiym7",
              actorName: "Mandragora",
              data: [
                { x: 2600, y: 2100, hidden: true },
                { x: 2400, y: 1900, hidden: true },
                { x: 2800, y: 1800, hidden: true },
                { x: 2800, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "jGNUUXyayvtHhL2d",
              actorName: "Drainberry Bush",
              data: [
                { x: 2700, y: 2000, hidden: true },
                { x: 2600, y: 1800, hidden: true },
                { x: 2400, y: 2000, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "S0JaQ5UdCQ2W3vHR",
      data: {
        name: "E1. Hazards",
        navName: "E1. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "QFR7IfOZ7xhulskg",
      data: {
        name: "E2. Hazards",
        navName: "E2. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "1YtP5qAHQ6iB91Oh",
      data: {
        name: "E3. Hazards",
        navName: "E3. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "D3HvxJV7jROgwrA2",
      data: {
        name: "F. The Disguised Forces",
        navName: "F. The Disguised Forces",
      },
      playerStartX: 1400,
      playerStartY: 3200,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9, 10, 11],
          tokens: [
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [
                { x: 1200, y: 1800, hidden: true },
                { x: 1000, y: 2200, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [
                { x: 1200, y: 1800, hidden: true },
                { x: 2000, y: 2600, hidden: true },
                { x: 1000, y: 2200, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "QQhT8T3Eqok5uCGo",
              actorName: "Blodeuwedd",
              data: [
                { x: 2000, y: 2600, hidden: true },
                { x: 1200, y: 1800, hidden: true },
                { x: 1000, y: 2200, hidden: true },
                { x: 2000, y: 1800, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "R9PpAKgFO9j9zumR",
      data: {
        name: "G1. Angry Animals",
        navName: "G1. Angry Animals",
      },
      playerStartX: 1400,
      playerStartY: 3200,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 2100, y: 2300, hidden: true },
                { x: 2100, y: 3000, hidden: true },
              ],
            },
            {
              actorId: "p8xtPV6eo0byJSiq",
              actorName: "Megaprimatus",
              data: [{ x: 1200, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [10, 11],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 1700, y: 3400, hidden: true },
                { x: 2100, y: 3000, hidden: true },
                { x: 2100, y: 2300, hidden: true },
                { x: 700, y: 2500, hidden: true },
              ],
            },
            {
              actorId: "p8xtPV6eo0byJSiq",
              actorName: "Megaprimatus",
              data: [{ x: 1200, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13],
          tokens: [
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 2100, y: 3300, hidden: true },
                { x: 2100, y: 3000, hidden: true },
                { x: 700, y: 2500, hidden: true },
                { x: 1700, y: 3400, hidden: true },
                { x: 2100, y: 2300, hidden: true },
                { x: 1900, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "p8xtPV6eo0byJSiq",
              actorName: "Megaprimatus",
              data: [{ x: 1200, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [14, 15],
          tokens: [
            {
              actorId: "p8xtPV6eo0byJSiq",
              actorName: "Megaprimatus",
              data: [
                { x: 1000, y: 1800, hidden: true },
                { x: 1900, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 2100, y: 3300, hidden: true },
                { x: 1900, y: 3500, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "p8xtPV6eo0byJSiq",
              actorName: "Megaprimatus",
              data: [
                { x: 2200, y: 2400, hidden: true },
                { x: 1000, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "BJAeXWyul3xknl2Z",
              actorName: "Gorilla",
              data: [
                { x: 1900, y: 3500, hidden: true },
                { x: 2100, y: 3300, hidden: true },
                { x: 1600, y: 1900, hidden: true },
                { x: 2000, y: 1800, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "NOrqjfBahDIbmyUX",
      data: {
        name: "G2. Unfortunate Meeting",
        navName: "G2. Unfortunate Meeting",
      },
      playerStartX: 1400,
      playerStartY: 3200,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9],
          tokens: [
            {
              actorId: "6Ls7Qh9bma7bhwF1",
              actorName: "Giant Slug",
              data: [{ x: 2100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [10, 11],
          tokens: [
            {
              actorId: "604uRygpgP2DoFYV",
              actorName: "Giant Stag Beetle",
              data: [{ x: 1600, y: 2600, hidden: true }],
            },
            {
              actorId: "6Ls7Qh9bma7bhwF1",
              actorName: "Giant Slug",
              data: [{ x: 2100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13],
          tokens: [
            {
              actorId: "604uRygpgP2DoFYV",
              actorName: "Giant Stag Beetle",
              data: [{ x: 1600, y: 2600, hidden: true }],
            },
            {
              actorId: "6Ls7Qh9bma7bhwF1",
              actorName: "Giant Slug",
              data: [{ x: 2100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [14, 15],
          tokens: [
            {
              actorId: "604uRygpgP2DoFYV",
              actorName: "Giant Stag Beetle",
              data: [
                { x: 1400, y: 3000, hidden: true },
                { x: 1600, y: 3500, hidden: true },
                { x: 1600, y: 2200, hidden: true },
                { x: 2700, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "6Ls7Qh9bma7bhwF1",
              actorName: "Giant Slug",
              data: [{ x: 2100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "604uRygpgP2DoFYV",
              actorName: "Giant Stag Beetle",
              data: [{ x: 1600, y: 2200, hidden: true }],
            },
            {
              actorId: "6Ls7Qh9bma7bhwF1",
              actorName: "Giant Slug",
              data: [
                { x: 1700, y: 3200, hidden: true },
                { x: 2100, y: 2100, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "qrVCa14Z6vyYaw9D",
      data: {
        name: "Opening The Door",
        navName: "Opening The Door",
      },
      playerStartX: 700,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "FUNY7EvFQXUqlmH6",
      data: {
        name: "H. Attunement",
        navName: "H. Attunement",
      },
      playerStartX: 1200,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "2G49yFcdbYTMg5xM",
      data: {
        name: "I. Defense",
        navName: "I. Defense",
      },
      playerStartX: 1400,
      playerStartY: 3200,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9, 10, 11],
          tokens: [
            {
              actorId: "uhicUTGZh0xIG64A",
              actorName: "Culdewen",
              data: [
                { x: 1400, y: 1800, hidden: true },
                { x: 1100, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "hdLfGpV6rKwi4tA3",
              actorName: "Awakened Tree",
              data: [
                { x: 2400, y: 1600, hidden: true },
                { x: 1200, y: 2400, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "hdLfGpV6rKwi4tA3",
              actorName: "Awakened Tree",
              data: [
                { x: 1400, y: 3100, hidden: true },
                { x: 2400, y: 1600, hidden: true },
                { x: 1200, y: 2400, hidden: true },
              ],
            },
            {
              actorId: "uhicUTGZh0xIG64A",
              actorName: "Culdewen",
              data: [
                { x: 1100, y: 1800, hidden: true },
                { x: 1200, y: 1600, hidden: true },
                { x: 1400, y: 1800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "hdLfGpV6rKwi4tA3",
              actorName: "Awakened Tree",
              data: [
                { x: 2700, y: 1700, hidden: true },
                { x: 1400, y: 3100, hidden: true },
                { x: 1100, y: 2600, hidden: true },
                { x: 2100, y: 1500, hidden: true },
              ],
            },
            {
              actorId: "uhicUTGZh0xIG64A",
              actorName: "Culdewen",
              data: [
                { x: 1100, y: 1800, hidden: true },
                { x: 1200, y: 1600, hidden: true },
                { x: 1000, y: 1500, hidden: true },
                { x: 1400, y: 1800, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "ydC0BoN4OcUupfHY",
      data: {
        name: "J. The Thorned Monarch Attacks",
        navName: "J. The Thorned Monarch Attacks",
      },
      playerStartX: 1400,
      playerStartY: 3200,
      scaling: [
        {
          tier: "7-8",
          cp: [8, 9],
          tokens: [
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1100, y: 1200, hidden: true },
                { x: 2800, y: 2800, hidden: true },
              ],
            },
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [10, 11],
          tokens: [
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 2800, y: 2800, hidden: true },
                { x: 1100, y: 1200, hidden: true },
                { x: 2700, y: 1600, hidden: true },
              ],
            },
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [12, 13],
          tokens: [
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1100, y: 1200, hidden: true },
                { x: 2700, y: 1600, hidden: true },
                { x: 2800, y: 2800, hidden: true },
                { x: 1400, y: 2600, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [14, 15],
          tokens: [
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1100, y: 1200, hidden: true },
                { x: 2700, y: 1600, hidden: true },
                { x: 2800, y: 2800, hidden: true },
                { x: 1400, y: 2600, hidden: true },
                { x: 2000, y: 2800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [16, 17],
          tokens: [
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1100, y: 1200, hidden: true },
                { x: 2700, y: 1600, hidden: true },
                { x: 2800, y: 2800, hidden: true },
                { x: 1400, y: 2600, hidden: true },
                { x: 2000, y: 2800, hidden: true },
                { x: 2100, y: 1100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "7-8",
          cp: [18],
          tokens: [
            {
              actorId: "GztYd2UqNq6MgC26",
              actorName: "Qxal, The Thorned Monarch",
              data: [{ x: 1100, y: 2100, hidden: true }],
            },
            {
              actorId: "bTmG49RT6ZekyH53",
              actorName: "Grimstalker",
              data: [
                { x: 1100, y: 1200, hidden: true },
                { x: 2700, y: 1600, hidden: true },
                { x: 2800, y: 2800, hidden: true },
                { x: 1400, y: 2600, hidden: true },
                { x: 2000, y: 2800, hidden: true },
                { x: 2100, y: 1100, hidden: true },
                { x: 800, y: 1700, hidden: true },
              ],
            },
          ],
        },
      ],
    },
  ],
};
