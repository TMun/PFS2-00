export default {
  scenarioLabel: "2-00 King in Thorns (1-2)",
  journalEntries: [
    "NMIsMBmsj6envUvU",
    "JjbVZqBQ6RXZShvy",
    "HgCOIgCSi8Argxli",
    "2VgP1ntdIVIksC1r",
    "3kOv8UCJzVEi8PAA",
    "Fifk8I4NVtsE4yp9",
    "QrLLLblyRReMdxIp",
    "qe71oDhuKx1mfe5A",
    "riXtUdUea9QKjXhq",
    "cCGLculeLY1sZcrk",
  ],
  scenarioMinLevel: 1,
  scenes: [
    {
      id: "asulbcUAgA6ewVtT",
      data: {
        name: "Encampment",
        navName: "Encampment",
      },
      playerStartX: 2000,
      playerStartY: 2900,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "iRxEIg1C4XCn9xtN",
              actorName: "Calisro Benarry",
              data: [{ x: 2100, y: 2100, hidden: false }],
            },
            {
              actorId: "GUnb3w2xfHMT9ewc",
              actorName: "Fola Barun",
              data: [{ x: 1800, y: 1900, hidden: false }],
            },
            {
              actorId: "EoaOGLJm2HjBXDwU",
              actorName: "Urwal",
              data: [{ x: 1300, y: 1900, hidden: false }],
            },
            {
              actorId: "S66102Wg5fu54g80",
              actorName: "Valais Durant",
              data: [{ x: 1400, y: 2600, hidden: false }],
            },
            {
              actorId: "FWMbhE4I272H3KL5",
              actorName: "Venture-Captain Bjersig Torrsen",
              data: [{ x: 2000, y: 2600, hidden: false }],
            },
            {
              actorId: "vb6V1RwYdIyPgIDv",
              actorName: "Venture-Captain Oraiah Tolal",
              data: [{ x: 1600, y: 2900, hidden: false }],
            },
          ],
        },
      ],
    },
    {
      id: "LN0OsuBxqYd35MtA",
      data: {
        name: "A. Mushroom Ring",
        navName: "A. Mushroom Ring",
      },
      playerStartX: 200,
      playerStartY: 1000,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
          tokens: [
            {
              actorId: "YZ1oHUZZ77cwFYDu",
              actorName: "Mushroom Ring (1-2)",
              data: [{ x: 500, y: 700, hidden: false }],
            },
            {
              actorId: "DRZ7spvlVj4ATeww",
              actorName: "Pugwampi",
              data: [
                { x: 200, y: 250, hidden: true },
                { x: 250, y: 250, hidden: true },
                { x: 300, y: 250, hidden: true },
                { x: 350, y: 250, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "Q2UYeYxTiwLSvsUF",
      data: {
        name: "B. It Talks",
        navName: "B. It Talks",
      },
      playerStartX: 1100,
      playerStartY: 2700,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "QmPqP9StFHfwfltR",
              actorName: "Awakened Ball Python",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
            {
              actorId: "oSzMl53XXlxKD9I2",
              actorName: "Viper",
              data: [
                { x: 2100, y: 2300, hidden: true },
                { x: 2000, y: 2100, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "oSzMl53XXlxKD9I2",
              actorName: "Viper",
              data: [
                { x: 2100, y: 2300, hidden: true },
                { x: 2000, y: 2100, hidden: true },
                { x: 2200, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "QmPqP9StFHfwfltR",
              actorName: "Awakened Ball Python",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "oSzMl53XXlxKD9I2",
              actorName: "Viper",
              data: [
                { x: 2300, y: 2100, hidden: true },
                { x: 2000, y: 2100, hidden: true },
                { x: 1900, y: 2000, hidden: true },
                { x: 2100, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "QmPqP9StFHfwfltR",
              actorName: "Awakened Ball Python",
              data: [{ x: 1900, y: 2200, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "QmPqP9StFHfwfltR",
              actorName: "Awakened Ball Python",
              data: [
                { x: 1900, y: 2200, hidden: true },
                { x: 2100, y: 2300, hidden: true },
              ],
            },
            {
              actorId: "oSzMl53XXlxKD9I2",
              actorName: "Viper",
              data: [
                { x: 2000, y: 2100, hidden: true },
                { x: 2200, y: 2100, hidden: true },
                { x: 1900, y: 2000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "oSzMl53XXlxKD9I2",
              actorName: "Viper",
              data: [
                { x: 2050, y: 1900, hidden: true },
                { x: 1900, y: 2000, hidden: true },
                { x: 2000, y: 2100, hidden: true },
                { x: 2200, y: 2100, hidden: true },
                { x: 2300, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "QmPqP9StFHfwfltR",
              actorName: "Awakened Ball Python",
              data: [
                { x: 2100, y: 2300, hidden: true },
                { x: 1900, y: 2200, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "wxnD3jXUojrqLuA1",
      data: {
        name: "C. Befuddled Pathfinders (7-8)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "h78Uw5F8Bi8qEZNx",
      data: {
        name: "C. Befuddled Pathfinders (3-6)",
        navName: "C. Befuddled Pathfinders",
        navigation: false,
      },
      scaling: [],
    },
    {
      id: "FVcQs8dVsf1CtEpo",
      data: {
        name: "C. Befuddled Pathfinders (1-2)",
        navName: "C. Befuddled Pathfinders",
        navigation: true,
      },
      playerStartX: 1400,
      playerStartY: 2700,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9, 10, 11],
          tokens: [
            {
              actorId: "xAgFHlDXMirirzc0",
              actorName: "Commoner",
              data: [{ x: 2100, y: 2900, hidden: true }],
            },
            {
              actorId: "0K1vk0NxNbRbvJXi",
              actorName: "Adept",
              data: [{ x: 2200, y: 2700, hidden: true }],
            },
            {
              actorId: "OBf0pkznve3GyRXk",
              actorName: "Apothecary",
              data: [{ x: 2300, y: 2900, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13, 14, 15],
          tokens: [
            {
              actorId: "0K1vk0NxNbRbvJXi",
              actorName: "Adept",
              data: [{ x: 2200, y: 2700, hidden: true }],
            },
            {
              actorId: "OBf0pkznve3GyRXk",
              actorName: "Apothecary",
              data: [{ x: 2300, y: 2900, hidden: true }],
            },
            {
              actorId: "xAgFHlDXMirirzc0",
              actorName: "Commoner",
              data: [
                { x: 2100, y: 2800, hidden: true },
                { x: 2100, y: 2900, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "0K1vk0NxNbRbvJXi",
              actorName: "Adept",
              data: [
                { x: 2200, y: 2700, hidden: true },
                { x: 2300, y: 2800, hidden: true },
              ],
            },
            {
              actorId: "OBf0pkznve3GyRXk",
              actorName: "Apothecary",
              data: [{ x: 2300, y: 2900, hidden: true }],
            },
            {
              actorId: "xAgFHlDXMirirzc0",
              actorName: "Commoner",
              data: [
                { x: 2100, y: 2800, hidden: true },
                { x: 2100, y: 2900, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "sLcY8PTEo55sgNhA",
      data: {
        name: "D. Enchanted Plants",
        navName: "D. Enchanted Plants",
      },
      playerStartX: 2000,
      playerStartY: 2600,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [{ x: 2500, y: 1900, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [
                { x: 2500, y: 1900, hidden: true },
                { x: 2700, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [
                { x: 2700, y: 2200, hidden: true },
                { x: 2500, y: 1900, hidden: true },
                { x: 2500, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [
                { x: 2700, y: 1900, hidden: true },
                { x: 2500, y: 1900, hidden: true },
                { x: 2500, y: 2100, hidden: true },
                { x: 2700, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17],
          tokens: [
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [
                { x: 2800, y: 2100, hidden: true },
                { x: 2500, y: 1900, hidden: true },
                { x: 2500, y: 2100, hidden: true },
                { x: 2700, y: 2200, hidden: true },
                { x: 2700, y: 1900, hidden: true },
              ],
            },
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [18],
          tokens: [
            {
              actorId: "7rTN95cGexlfJZCx",
              actorName: "Yellow Musk Thrall",
              data: [
                { x: 2800, y: 2100, hidden: true },
                { x: 2500, y: 1900, hidden: true },
                { x: 2500, y: 2100, hidden: true },
                { x: 2700, y: 2200, hidden: true },
                { x: 2700, y: 1900, hidden: true },
                { x: 2800, y: 1800, hidden: true },
              ],
            },
            {
              actorId: "qjSaBCr05D3r6zfW",
              actorName: "Yellow Musk Creeper",
              data: [{ x: 2700, y: 2000, hidden: true }],
            },
          ],
        },
      ],
    },
    {
      id: "S0JaQ5UdCQ2W3vHR",
      data: {
        name: "E1. Hazards",
        navName: "E1. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "QFR7IfOZ7xhulskg",
      data: {
        name: "E2. Hazards",
        navName: "E2. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "1YtP5qAHQ6iB91Oh",
      data: {
        name: "E3. Hazards",
        navName: "E3. Hazards",
      },
      playerStartX: 300,
      playerStartY: 100,
      scaling: [],
    },
    {
      id: "D3HvxJV7jROgwrA2",
      data: {
        name: "F. The Disguised Forces",
        navName: "F. The Disguised Forces",
      },
      playerStartX: 1300,
      playerStartY: 2200,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [{ x: 2000, y: 2800, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [
                {
                  x: 2000,
                  y: 2800,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [{ x: 2000, y: 2800, hidden: true }],
            },
            {
              actorId: "onJEeOhqP9N5Aw3a",
              actorName: "Spear Frog",
              data: [{ x: 1900, y: 2500, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "onJEeOhqP9N5Aw3a",
              actorName: "Spear Frog",
              data: [{ x: 1850, y: 3000, hidden: true }],
            },
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [
                {
                  x: 2000,
                  y: 2800,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "wxlHabgHSSAjfefX",
              actorName: "Mudwretch",
              data: [{ x: 2000, y: 2800, hidden: true }],
            },
            {
              actorId: "onJEeOhqP9N5Aw3a",
              actorName: "Spear Frog",
              data: [
                { x: 1900, y: 2500, hidden: true },
                { x: 1850, y: 3000, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "R9PpAKgFO9j9zumR",
      data: {
        name: "G1. Angry Animals",
        navName: "G1. Angry Animals",
      },
      playerStartX: 1200,
      playerStartY: 2700,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "anaCS2i2FAIxDoRF",
              actorName: "Cythnigot",
              data: [
                { x: 2400, y: 2800, hidden: true },
                { x: 1600, y: 2000, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "anaCS2i2FAIxDoRF",
              actorName: "Cythnigot",
              data: [
                {
                  x: 2400,
                  y: 2800,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
                {
                  x: 1600,
                  y: 2000,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "anaCS2i2FAIxDoRF",
              actorName: "Cythnigot",
              data: [
                { x: 1600, y: 2000, hidden: true },
                { x: 2200, y: 2400, hidden: true },
                { x: 2400, y: 2800, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "anaCS2i2FAIxDoRF",
              actorName: "Cythnigot",
              data: [
                {
                  x: 1600,
                  y: 2000,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
                {
                  x: 2200,
                  y: 2400,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
                {
                  x: 2400,
                  y: 2800,
                  hidden: true,
                  flags: { sigil: { max: "+5" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "anaCS2i2FAIxDoRF",
              actorName: "Cythnigot",
              data: [
                { x: 2200, y: 2400, hidden: true },
                { x: 1600, y: 2000, hidden: true },
                { x: 2400, y: 2800, hidden: true },
                { x: 900, y: 2200, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "NOrqjfBahDIbmyUX",
      data: {
        name: "G2. Unfortunate Meeting",
        navName: "G2. Unfortunate Meeting",
      },
      playerStartX: 2200,
      playerStartY: 2800,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "wMjhBDEDk2I0cyG2",
              actorName: "Ember Fox",
              data: [{ x: 1800, y: 1800, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "wMjhBDEDk2I0cyG2",
              actorName: "Ember Fox",
              data: [
                {
                  x: 1800,
                  y: 1800,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "wMjhBDEDk2I0cyG2",
              actorName: "Ember Fox",
              data: [
                {
                  x: 1800,
                  y: 1800,
                  hidden: true,
                  flags: { sigil: { adjustments: ["elite"] } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "wMjhBDEDk2I0cyG2",
              actorName: "Ember Fox",
              data: [{ x: 2100, y: 2000, hidden: true }],
            },
            {
              actorId: "iF6Zhmxrkrn7RUAv",
              actorName: "Fire Mephit",
              data: [{ x: 1700, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "wMjhBDEDk2I0cyG2",
              actorName: "Ember Fox",
              data: [
                {
                  x: 2100,
                  y: 2000,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
            {
              actorId: "iF6Zhmxrkrn7RUAv",
              actorName: "Fire Mephit",
              data: [{ x: 1700, y: 2100, hidden: true }],
            },
          ],
        },
      ],
    },
    {
      id: "qrVCa14Z6vyYaw9D",
      data: {
        name: "Opening The Door",
        navName: "Opening The Door",
      },
      playerStartX: 700,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "FUNY7EvFQXUqlmH6",
      data: {
        name: "H. Attunement",
        navName: "H. Attunement",
      },
      playerStartX: 1200,
      playerStartY: 500,
      scaling: [],
    },
    {
      id: "2G49yFcdbYTMg5xM",
      data: {
        name: "I. Defense",
        navName: "I. Defense",
      },
      playerStartX: 2300,
      playerStartY: 2500,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [{ x: 1400, y: 2100, hidden: true }],
            },
            {
              actorId: "WlHAPeoaotJfaju7",
              actorName: "Vexgit",
              data: [
                { x: 1200, y: 1600, hidden: true },
                { x: 1250, y: 1650, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [
                {
                  x: 1400,
                  y: 2100,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
            {
              actorId: "WlHAPeoaotJfaju7",
              actorName: "Vexgit",
              data: [
                {
                  x: 1200,
                  y: 1600,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 1250,
                  y: 1650,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "DRZ7spvlVj4ATeww",
              actorName: "Pugwampi",
              data: [{ x: 1650, y: 2100, hidden: true }],
            },
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [{ x: 1400, y: 2100, hidden: true }],
            },
            {
              actorId: "WlHAPeoaotJfaju7",
              actorName: "Vexgit",
              data: [
                { x: 1200, y: 1600, hidden: true },
                { x: 1250, y: 1650, hidden: true },
                { x: 1250, y: 1600, hidden: true },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "DRZ7spvlVj4ATeww",
              actorName: "Pugwampi",
              data: [{ x: 1650, y: 2100, hidden: true }],
            },
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [
                {
                  x: 1400,
                  y: 2100,
                  hidden: true,
                  flags: { sigil: { max: "+15" } },
                },
              ],
            },
            {
              actorId: "WlHAPeoaotJfaju7",
              actorName: "Vexgit",
              data: [
                {
                  x: 1200,
                  y: 1600,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 1250,
                  y: 1650,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
                {
                  x: 1250,
                  y: 1600,
                  hidden: true,
                  flags: { sigil: { max: "+10" } },
                },
              ],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17, 18],
          tokens: [
            {
              actorId: "DRZ7spvlVj4ATeww",
              actorName: "Pugwampi",
              data: [
                { x: 1350, y: 2350, hidden: true },
                { x: 1650, y: 2100, hidden: true },
              ],
            },
            {
              actorId: "86FeG5xSms4fjs2q",
              actorName: "Nuglub (PFS 2-00)",
              data: [{ x: 1400, y: 2100, hidden: true }],
            },
            {
              actorId: "WlHAPeoaotJfaju7",
              actorName: "Vexgit",
              data: [
                { x: 1200, y: 1600, hidden: true },
                { x: 1250, y: 1650, hidden: true },
                { x: 1250, y: 1600, hidden: true },
                { x: 1250, y: 1650, hidden: true },
              ],
            },
          ],
        },
      ],
    },
    {
      id: "ydC0BoN4OcUupfHY",
      data: {
        name: "J. The Thorned Monarch Attacks",
        navName: "J. The Thorned Monarch Attacks",
      },
      playerStartX: 1900,
      playerStartY: 1600,
      scaling: [
        {
          tier: "1-2",
          cp: [8, 9],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 1100, y: 1700, hidden: true },
                { x: 1300, y: 2500, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [10, 11],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 1300, y: 2500, hidden: true },
                { x: 1100, y: 1700, hidden: true },
                { x: 2000, y: 2600, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [12, 13],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 1300, y: 2500, hidden: true },
                { x: 1100, y: 1700, hidden: true },
                { x: 2000, y: 2600, hidden: true },
                { x: 1600, y: 2800, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [14, 15],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 2000, y: 2600, hidden: true },
                { x: 1100, y: 1700, hidden: true },
                { x: 1600, y: 2800, hidden: true },
                { x: 1300, y: 2500, hidden: true },
                { x: 2600, y: 2200, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [16, 17],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 2000, y: 2600, hidden: true },
                { x: 1100, y: 1700, hidden: true },
                { x: 1600, y: 2800, hidden: true },
                { x: 1300, y: 2500, hidden: true },
                { x: 2600, y: 2200, hidden: true },
                { x: 1200, y: 1500, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
        {
          tier: "1-2",
          cp: [18],
          tokens: [
            {
              actorId: "peAEse2rdwhp2Xhc",
              actorName: "Mitflit",
              data: [
                { x: 2000, y: 2600, hidden: true },
                { x: 1100, y: 1700, hidden: true },
                { x: 1600, y: 2800, hidden: true },
                { x: 1300, y: 2500, hidden: true },
                { x: 2600, y: 2200, hidden: true },
                { x: 1200, y: 1500, hidden: true },
                { x: 1500, y: 2600, hidden: true },
              ],
            },
            {
              actorId: "JYdQg320pr6NiuSr",
              actorName: "Quickling (PFS 2-00)",
              data: [{ x: 800, y: 2100, hidden: true }],
            },
          ],
        },
      ],
    },
  ],
};
